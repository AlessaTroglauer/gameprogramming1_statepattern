﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonicSearch_ClassBased : MonoBehaviour
{
    [SerializeField]
    private ISonicState currentState;

    [SerializeField]
    public StandingState standingState = new StandingState();

    [SerializeField]
    public WalkingState walkingState = new WalkingState();

    [SerializeField]
    public RunningState runningState = new RunningState();

    public SpriteRenderer spriteRenderer;
    public Sprite standingSprite;
    public Sprite walkingSprite;
    public Sprite runningSprite;


    private void OnEnable()
    {
        currentState = standingState;
    }

    private void Update()
    {
        currentState = currentState.DoState(this);
    }
}
