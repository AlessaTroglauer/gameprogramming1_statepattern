﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingState : ISonicState
{
    public ISonicState DoState(SonicSearch_ClassBased sonic)
    {
        Walk(sonic);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            return sonic.runningState;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            return sonic.standingState;
        }
        else
        {
            return sonic.walkingState;
        }

    }

    private void Walk(SonicSearch_ClassBased sonic)
    {
        sonic.spriteRenderer.sprite = sonic.walkingSprite;
    }
}
