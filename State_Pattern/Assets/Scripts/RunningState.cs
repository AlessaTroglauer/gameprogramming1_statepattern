﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningState : ISonicState
{
    public ISonicState DoState(SonicSearch_ClassBased sonic)
    {
        Run(sonic);

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            return sonic.standingState;
        }

        if (Input.GetKey(KeyCode.D))
        {
            return sonic.walkingState;
        }
        else
        {
            return sonic.runningState;
        }
    }

    private void Run(SonicSearch_ClassBased sonic)
    {
        sonic.spriteRenderer.sprite = sonic.runningSprite;
    }
}
