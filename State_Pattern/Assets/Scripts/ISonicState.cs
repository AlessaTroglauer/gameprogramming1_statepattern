﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISonicState
{ 
    ISonicState DoState(SonicSearch_ClassBased sonic);
}
