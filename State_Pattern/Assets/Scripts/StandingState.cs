﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandingState : ISonicState
{
    public ISonicState DoState(SonicSearch_ClassBased sonic)
    {
        Stand(sonic);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            return sonic.runningState;
        }

        if (Input.GetKey(KeyCode.D))
        {
            return sonic.walkingState;
        }
        else
        {
            return sonic.standingState;
        }
    }

    private void Stand(SonicSearch_ClassBased sonic)
    {
        sonic.spriteRenderer.sprite = sonic.standingSprite;
    }

}
